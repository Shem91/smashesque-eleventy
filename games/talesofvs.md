---
title: "Tales of Vs."
img: /games/img/talesofvs.png
type:
  - Full Game
tags:
  - Anime
  - JRPG
  - 3D
  - Casual
platforms:
  - PSP
maxplayers:
  - 4
rostersize:
  - 35
video: https://www.youtube-nocookie.com/embed/zihqxecmHAM
longdescription:
  - 'Tales of VS. (テイルズ オブ バーサス) is a crossover fighting game featuring various characters across the Tales video game series. The game takes the basic fighting engine from the main series of Tales video games, which is called the "Linear Motion Battle System".'
---
