---
title: "Megabyte Punch"
img: /games/img/megabytepunch.png
type:
  - Full Game
tags:
  - Indie
  - Custom Characters
  - 3D
  - Casual
platforms:
  - PC
  - Switch
maxplayers:
  - 4
rostersize:
  - N/A
video: https://www.youtube-nocookie.com/embed/3M1yIiqr_oU
longdescription:
  - 'Megabyte Punch is a fighting/beat ‘em up game in which players build their own fighter. Parts have powers and bonuses such as gun arms for a shoot ability. The game features multiplayer battles.'
---
