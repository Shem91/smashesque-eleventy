---
title: "Playstation All-Stars Battle Royale"
img: /games/img/psasbr.png
type:
  - Full Game
tags:
  - Crossover
  - Technical
  - Cameos
  - 3D
  - Competitive
platforms:
  - PS3
  - PS Vita
maxplayers:
  - 4
rostersize:
  - 24
video: https://www.youtube.com/embed/2-42m3VYKF0
longdescription:
  - 'PlayStation All-Stars Battle Royale is a crossover fighting video game developed by SuperBot Entertainment, in conjunction with SCE Santa Monica Studio, and published by Sony Computer Entertainment. It features various characters drawn from different PlayStation video game franchises competing against each other in multiplayer battles.'
---
