---
title: "Sonic Rivals 2 (Knock Out)"
img: /games/img/sonicrivals2.png
type:
  - Game Mode
tags:
  - Casual
  - 3D
  - Arguable
platforms:
  - PSP
maxplayers:
  - 2
rostersize:
  - 8
video: https://www.youtube-nocookie.com/embed/A4pPC86IWfY
longdescription:
  - 'Sonic Rivals 2 features platform gameplay similar to other entries in the series. Rivals 2 specifically contains an arena combat mode called Knock Out, in which each player starts with three rings. Players must knock all of the rings out of their opponent and then attack them to win.'
---
