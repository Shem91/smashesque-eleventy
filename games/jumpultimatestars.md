---
title: "Jump! Ultimate Stars"
img: /games/img/jumpus.png
type:
  - Full Game
tags:
  - Crossover
  - Anime
  - Casual
  - Pixel Art
platforms:
  - DS
maxplayers:
  - 4
rostersize:
  - 54
video: https://www.youtube-nocookie.com/embed/GcFrKseh_pE
longdescription:
  - 'Jump Ultimate Stars is a fighting video game developed by Ganbarion and published by Nintendo for the Nintendo DS. Jump Ultimate Stars has been changed slightly from the gameplay of Jump Super Stars. Jump Ultimate Stars gives battle characters the ability to dash and to do a new attack while guarding, which, instead of breaking the guard of the opponent, forces them to change characters, and can be identified by a green glow coming out of the characters which executes it.'
---
