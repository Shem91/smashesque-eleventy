---
title: "Battle Stadium D.O.N."
img: /games/img/bsd.png
type:
  - Full Game
tags:
  - Crossover
  - Anime
  - Casual
  - '3D'
platforms:
  - GameCube
  - PS2
maxplayers:
  - 4
rostersize:
  - 20
video: https://www.youtube-nocookie.com/embed/k5BmoXhWHpM
longdescription:
  - Battle Stadium D.O.N (バトルスタジアムＤＯＮ) is a Japanese crossover fighting game. The "D.O.N." in the game's title is derived from Dragon Ball, One Piece, and Naruto, the three manga series published by Weekly Shōnen Jump upon which the game is based.
---
