---
title: "Roof Rage"
img: /games/img/roofrage.png
type:
  - Full Game
tags:
  - Indie
  - Pixel Art
  - Technical
platforms:
  - PC
maxplayers:
  - 8
rostersize:
  - 13
video: https://www.youtube-nocookie.com/embed/mhxIYyBCoFA
longdescription:
  - 'Roof Rage is a Martial Arts Platform Fighter featuring epic rooftop battles. It is both a party game and a competitive gameThe scale and speed of Roof Rage allows you to be creative, always producing new combos and pushing the gameplay a little bit further.'
---
