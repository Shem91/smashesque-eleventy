---
title: "Lethal League Blaze"
img: /games/img/lethalleagueblaze.png
type:
  - Full Game
tags:
  - Sports
  - Technical
  - Competitive
  - Arguable
platforms:
  - PS4
  - Xbox One
  - PC
maxplayers:
  - 2
rostersize:
  - 7
video: https://www.youtube-nocookie.com/embed/nNbvx6MFKbk
longdescription:
  - 'Lethal League Blaze is a remake of the original game, which features fully 3D character models and animations as well as new playable characters.'
---
