---
title: "Dynasty Feud"
img: /games/img/dynastyfeud.png
type:
  - Full Game
tags:
  - Pixel Art
  - Casual
  - Guns
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - 40
video: https://www.youtube-nocookie.com/embed/_KXPpZW0JZ8
longdescription:
  - Dynasty Feud features a unique one-hit kill mechanic in which players form a "dynasty" of five different characters. The game features a wide variety of characters available in each dynasty.
---
