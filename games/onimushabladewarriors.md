---
title: "Onimusha Blade Warriors"
img: /games/img/onimushabladewarriors.png
type:
  - Full Game
tags:
  - Retro
  - Cameos
  - Spinoff
platforms:
  - PS2
maxplayers:
  - 4
rostersize:
  - 23
video: https://www.youtube.com/embed/cDRgE1QLUfY
longdescription:
  - 'Onimusha Blade Warriors (鬼武者 無頼伝) is a 2D fighter game featuring many of the characters of the Onimusha franchise. Players may use standard sword fighting with combos, blocks, block-breaking kicks, jump, switch planes (the different levels of the fighting area), use various items, and disarm their opponents. It also includes the original aspects of the Onimusha series, including the absorption of souls and special elemental attacks (lightning, fire, and wind).'
---
