/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("workbox-v4.3.1/workbox-sw.js");
workbox.setConfig({modulePathPrefix: "workbox-v4.3.1"});

workbox.core.setCacheNameDetails({prefix: "eleventy-plugin-pwa"});

workbox.core.skipWaiting();

workbox.core.clientsClaim();

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "192.png",
    "revision": "da3ef4af26047ab723f266ac7297d88c"
  },
  {
    "url": "512.png",
    "revision": "75e562a9dda555f351e5eec04f2d83e4"
  },
  {
    "url": "apple-touch-icon.png",
    "revision": "e7fdbe124e239b29a6d4f00068fe5e7f"
  },
  {
    "url": "games/battlestadiumdon/index.html",
    "revision": "0767032f6796c1e54d593af416f2246a"
  },
  {
    "url": "games/brawlhalla/index.html",
    "revision": "1bf008a652076c223928c4fc7b005c63"
  },
  {
    "url": "games/brawlout/index.html",
    "revision": "24097c79fd37e753fefc73d784e4b41a"
  },
  {
    "url": "games/bwbushido/index.html",
    "revision": "446371f271a76620128ab7d06fe4e2fd"
  },
  {
    "url": "games/cartoonnetwork/index.html",
    "revision": "a7f8b9230da7e6545d73592e78b64692"
  },
  {
    "url": "games/cartoonnetworkxl/index.html",
    "revision": "f3aa2a96acfefd1ab72dd2466957b616"
  },
  {
    "url": "games/digimonbattlespirit/index.html",
    "revision": "31987c69ba6b6a7ba721eea22d039c11"
  },
  {
    "url": "games/digimonbattlespirit2/index.html",
    "revision": "7cfee2e26383a4ada289460c7b86a8b3"
  },
  {
    "url": "games/digimonrumblearena/index.html",
    "revision": "ba02da3955dbfb0b6d897f1194d6e059"
  },
  {
    "url": "games/digimonrumblearena2/index.html",
    "revision": "18c55a6d9c06a39845811f2b1ebf3d18"
  },
  {
    "url": "games/dimensionsvs/index.html",
    "revision": "91889e05a828617998ce2aace30f0820"
  },
  {
    "url": "games/draglade/index.html",
    "revision": "cbfc0b479c6f47602acc5f0ff19e7de5"
  },
  {
    "url": "games/draglade2/index.html",
    "revision": "c02e24260b4c6c7d86b66e154a1bbaf2"
  },
  {
    "url": "games/dreammixtvworldfighters/index.html",
    "revision": "4fbdfce8570a9302e9c2baf6aaf681c8"
  },
  {
    "url": "games/duckgame/index.html",
    "revision": "a46d48109126b448b8716bacf22ab3e3"
  },
  {
    "url": "games/dynastyfued/index.html",
    "revision": "e49d0d0129006cb6718186b918ca03a6"
  },
  {
    "url": "games/fighties/index.html",
    "revision": "9c88318756a53b120102b0079105ca22"
  },
  {
    "url": "games/grabity/index.html",
    "revision": "7c86cd2ddb8b9d6a12832e4b90e3d9ad"
  },
  {
    "url": "games/icons combat arena/index.html",
    "revision": "cec9968cc365f548ab2750472ef04764"
  },
  {
    "url": "games/img/brawlhalla.png",
    "revision": "7f44c0a09b9b1fcdb4ffc3c7852b0d7d"
  },
  {
    "url": "games/img/brawlout.png",
    "revision": "bd8b3eeb5a316dde0f80a9f07524794d"
  },
  {
    "url": "games/img/bsd.png",
    "revision": "4ae174b7377c7e2529965d305cd3f615"
  },
  {
    "url": "games/img/bwbushido.png",
    "revision": "396e52db6410fd52aac37953859940a5"
  },
  {
    "url": "games/img/cnpte.png",
    "revision": "9f081bcfc9d0694afa047c11173b744f"
  },
  {
    "url": "games/img/cnptexl.png",
    "revision": "9add21b6dfe173896c1259e684262c12"
  },
  {
    "url": "games/img/dbs.png",
    "revision": "d57469906a83615c7986972a02300863"
  },
  {
    "url": "games/img/dbs2.png",
    "revision": "235f9bbf3461ce5b33575f0ae9438cd4"
  },
  {
    "url": "games/img/dmtvwf.png",
    "revision": "813a0064297209c3c0f168fbc98c64ce"
  },
  {
    "url": "games/img/dra.png",
    "revision": "4614c72f4216dc6bb5d9e5cbf1f5e542"
  },
  {
    "url": "games/img/dra2.png",
    "revision": "cdbd2f8d7937fb62fb57ec57f3bbbd05"
  },
  {
    "url": "games/img/draglade.png",
    "revision": "dbc8d8733ec582345c3f15e54d7ec46f"
  },
  {
    "url": "games/img/draglade2.png",
    "revision": "974c678f232876a3ce111bf2a28b4919"
  },
  {
    "url": "games/img/duckgame.png",
    "revision": "0a902e21f35213383822fc92e6447e2b"
  },
  {
    "url": "games/img/dvs.png",
    "revision": "084154795c54b3889c135547569858f9"
  },
  {
    "url": "games/img/dynastyfeud.png",
    "revision": "f24465d11a95190740680fb026e9beb6"
  },
  {
    "url": "games/img/fighties.png",
    "revision": "9b3386d14aa1f38ae561c87744609c3a"
  },
  {
    "url": "games/img/grabity.png",
    "revision": "dfb42aeedf49c08991084b8989b9abd7"
  },
  {
    "url": "games/img/icons.png",
    "revision": "6fc69f851d36cc0ba252f24f224a45cf"
  },
  {
    "url": "games/img/indiepogo.png",
    "revision": "ad1285568b8f7e3edd8d2a00e7bc011e"
  },
  {
    "url": "games/img/jumpss.png",
    "revision": "d0e2e7989ef0c98e8ddee0cf77f07c6c"
  },
  {
    "url": "games/img/jumpus.png",
    "revision": "35e64c3d778ac12cbdb4277c3d5a0e79"
  },
  {
    "url": "games/img/kungfupanda.png",
    "revision": "ed418ba581ea1758d64477819809ba53"
  },
  {
    "url": "games/img/legobrawls.jpg",
    "revision": "489ab9ca8ab1e6da6176d3f38fa736d7"
  },
  {
    "url": "games/img/lethalleague.png",
    "revision": "d90dba09153d88f7f62330413128ef21"
  },
  {
    "url": "games/img/lethalleagueblaze.png",
    "revision": "e39ea371fa77f6aa256777ab85297d68"
  },
  {
    "url": "games/img/logo.png",
    "revision": "705b06c0cf887f2933a72b7a8feebdfd"
  },
  {
    "url": "games/img/megabytepunch.png",
    "revision": "f213fb8b46620c4e080fe84bd8ac55cd"
  },
  {
    "url": "games/img/mysticmelee.png",
    "revision": "689219c9ef44b20dbd62504b93090777"
  },
  {
    "url": "games/img/nidhogg.png",
    "revision": "f6730630dc889bec4029bca27c49e657"
  },
  {
    "url": "games/img/nidhogg2.png",
    "revision": "84705ac8583161f5249e1df29e8270fa"
  },
  {
    "url": "games/img/onimushabladewarriors.png",
    "revision": "e58cfda271beecb34b2802b63098105f"
  },
  {
    "url": "games/img/outfoxies.png",
    "revision": "081c6afd5e73d8521d6c59a20f35d409"
  },
  {
    "url": "games/img/psasbr.png",
    "revision": "dd44df2c41e5e5dd827e7bb7c9675438"
  },
  {
    "url": "games/img/rivalsofaether.png",
    "revision": "00977ad5bda4b5c315506cae03f95d1f"
  },
  {
    "url": "games/img/roofrage.png",
    "revision": "cf0acfebe02b4c3998ca349f0bf16e9c"
  },
  {
    "url": "games/img/rumblearena.png",
    "revision": "114d93cc8c1c90809bc32131723fdf8e"
  },
  {
    "url": "games/img/samuraigunn.png",
    "revision": "dbc4f0aa20521645ca004af52cfdd352"
  },
  {
    "url": "games/img/samuraigunn2.png",
    "revision": "430e976a44c01d5323e0565654ab312a"
  },
  {
    "url": "games/img/shovelknightshowdown.png",
    "revision": "3f3feccae8e5b49023a9f223f36b91cd"
  },
  {
    "url": "games/img/skyscrappers.png",
    "revision": "41627a0f7077bf4c9c143147090a1c18"
  },
  {
    "url": "games/img/slamland.png",
    "revision": "a75adc346489ec6521fc6481f9de62b4"
  },
  {
    "url": "games/img/slapcity.png",
    "revision": "14f387978299ade1dd518d18fb455b40"
  },
  {
    "url": "games/img/smallarms.png",
    "revision": "d153b319cea550378f2ea702cd1e7bec"
  },
  {
    "url": "games/img/sonicrivals2.png",
    "revision": "cce1fffb5103645ebe835677f235d4aa"
  },
  {
    "url": "games/img/spbf.png",
    "revision": "d3b4a287ccc3299fd7e2a881c3d1b069"
  },
  {
    "url": "games/img/sscb.jpg",
    "revision": "29eb7fbf80dc1c0986827ec9b1a01da7"
  },
  {
    "url": "games/img/starwars.png",
    "revision": "28ab252e0f0938e976180be02204318e"
  },
  {
    "url": "games/img/superfightersdeluxe.png",
    "revision": "630ed29c2d0070027c960eba80ef54e8"
  },
  {
    "url": "games/img/talesofvs.png",
    "revision": "2c67914cf2201ce79060d3d3495f8cb9"
  },
  {
    "url": "games/img/theoutfoxies.png",
    "revision": "081c6afd5e73d8521d6c59a20f35d409"
  },
  {
    "url": "games/img/tmntsmashup.png",
    "revision": "d793af61887267ddeff4e4f634d6e0f2"
  },
  {
    "url": "games/img/towerfallascension.png",
    "revision": "1cd3bdded7d316f79ceeddf23a01e31e"
  },
  {
    "url": "games/img/viewtifuljoeredhotrumble.png",
    "revision": "0354800fbdc551f8f763a121b9d020b7"
  },
  {
    "url": "games/indiepogo/index.html",
    "revision": "f481d7fb432691f0f861e69c6f61652f"
  },
  {
    "url": "games/jumpsuperstars/index.html",
    "revision": "23703fcc716d5d33b15e5a55dcc1bdf8"
  },
  {
    "url": "games/jumpultimatestars/index.html",
    "revision": "63a29f1528bce23c7b8738d0be225b06"
  },
  {
    "url": "games/kungfupanda/index.html",
    "revision": "7a1c78251a3048a46a37503f2010edaa"
  },
  {
    "url": "games/legobrawls/index.html",
    "revision": "041376fed23f8a7d3924e914133882e5"
  },
  {
    "url": "games/lethalleague/index.html",
    "revision": "abd264bb28d5b8dcfd31bcc5f611b43b"
  },
  {
    "url": "games/lethalleagueblaze/index.html",
    "revision": "e874e3bf36e26c8eed5e41fa9b1d6e4d"
  },
  {
    "url": "games/megabytepunch/index.html",
    "revision": "5e8a2f6cabf3239dcd67bd1fd5169967"
  },
  {
    "url": "games/mysticmelee/index.html",
    "revision": "b9e0873c8ca27bafc241ab9f24a65068"
  },
  {
    "url": "games/nidhogg/index.html",
    "revision": "6c7c0c06ea195c1dd6ece4e7b4c4d16e"
  },
  {
    "url": "games/nidhogg2/index.html",
    "revision": "b90276eedf37398d140c3d2f8af12173"
  },
  {
    "url": "games/onimushabladewarriors/index.html",
    "revision": "7358037878b89abebb8ca82052f84789"
  },
  {
    "url": "games/outfoxies/index.html",
    "revision": "6668e59dd6a426591117b3a2beb40164"
  },
  {
    "url": "games/playstationallstars/index.html",
    "revision": "1818931121b3dae99593f7173b4d4322"
  },
  {
    "url": "games/rivalsofaether/index.html",
    "revision": "2fd3b1d76327e1ddfdae4e6d653985d6"
  },
  {
    "url": "games/roofrage/index.html",
    "revision": "466a18173d94f794c167aa8570038da6"
  },
  {
    "url": "games/rumblearena/index.html",
    "revision": "e784f5138243d05f91da751eea9fa6ed"
  },
  {
    "url": "games/samuraigunn/index.html",
    "revision": "1bd2a2e6fafae82977c29ad353290fa2"
  },
  {
    "url": "games/samuraigunn2/index.html",
    "revision": "d2581646c22dfe6ef7d9c2555923ab77"
  },
  {
    "url": "games/shovelknightshowdown/index.html",
    "revision": "cba8ff687570aad44c83fd8202e97002"
  },
  {
    "url": "games/skyscrappers/index.html",
    "revision": "083871d088c17d2fa172eef96a619132"
  },
  {
    "url": "games/slamland/index.html",
    "revision": "0974e93e4d499701def3c86e70144d41"
  },
  {
    "url": "games/slapcity/index.html",
    "revision": "d895fd2ed9f6d83ddee52d71c6342a5e"
  },
  {
    "url": "games/smallarms/index.html",
    "revision": "e90afbbac8c06c50116efa874595ff94"
  },
  {
    "url": "games/sonicrivals2/index.html",
    "revision": "ea62b6a15a59b6bbd33e06e7e55cb80b"
  },
  {
    "url": "games/starwarsforceunleashed2/index.html",
    "revision": "9aebdbc3a4899433c60b853cc2efcc34"
  },
  {
    "url": "games/superfightersdeluxe/index.html",
    "revision": "39d6ac31181071f83f9e2845e963a89b"
  },
  {
    "url": "games/superpoweredbattlefriends/index.html",
    "revision": "b7d2aa48b92d248de38936ad90e899d5"
  },
  {
    "url": "games/supersmashclashbrothers/index.html",
    "revision": "38327a8fd2a6528832238db572cfc0e8"
  },
  {
    "url": "games/talesofvs/index.html",
    "revision": "8bdc56421edec9b6fbea363e9990adcb"
  },
  {
    "url": "games/tmntsmashup/index.html",
    "revision": "d0c9d7f17e5145f3b20a007fe0620c14"
  },
  {
    "url": "games/towerfallascension/index.html",
    "revision": "15196b4a28ef977b042de10cc3f42bc0"
  },
  {
    "url": "games/viewtifuljoeredhotrumble/index.html",
    "revision": "c083be15a3ac004988dd6e4b395f8ea6"
  },
  {
    "url": "index.html",
    "revision": "f4e2b513d5c860fce50ab9227008ccb1"
  },
  {
    "url": "manifest.json",
    "revision": "1b54597c8c5d52a551f8020542c533c0"
  },
  {
    "url": "platforms/3DS/index.html",
    "revision": "51e3c3d85d398d26d0288141533782f4"
  },
  {
    "url": "platforms/Arcade/index.html",
    "revision": "298cad0c724713a40c53901db37005b8"
  },
  {
    "url": "platforms/DS/index.html",
    "revision": "a018fef346f84645f11d151785106d70"
  },
  {
    "url": "platforms/GameCube/index.html",
    "revision": "7426ff2b580ef52ed23c33458a9e8021"
  },
  {
    "url": "platforms/GBA/index.html",
    "revision": "4fba5c2f9dee1c352fb41640b8026d19"
  },
  {
    "url": "platforms/index.html",
    "revision": "e858e6b51834a5e209ea57dcca9e7835"
  },
  {
    "url": "platforms/Mobile/index.html",
    "revision": "ffec6d3e29a0d467d27b810115361fb4"
  },
  {
    "url": "platforms/Ouya/index.html",
    "revision": "a8e95a562eb895ac67fc05f5fc1a6b40"
  },
  {
    "url": "platforms/PC/index.html",
    "revision": "7011d44a2dcd5dc946e8785c322e9cb9"
  },
  {
    "url": "platforms/PS Vita/index.html",
    "revision": "d2c0c369d4e47f8e1d9ba1010ccb8100"
  },
  {
    "url": "platforms/PS1/index.html",
    "revision": "ca5376a1de5fb92ae51657dc32875325"
  },
  {
    "url": "platforms/PS2/index.html",
    "revision": "0e0972d6120dc453594c1c7b2f63ece8"
  },
  {
    "url": "platforms/PS3/index.html",
    "revision": "461711d7d1f8496509b38b355c69293c"
  },
  {
    "url": "platforms/PS4/index.html",
    "revision": "1e173d8da2f251296d882ff409333093"
  },
  {
    "url": "platforms/PSP/index.html",
    "revision": "e93450ee7a58086611bb15d83ce6fe25"
  },
  {
    "url": "platforms/Switch/index.html",
    "revision": "92c1f89ff87c6b70cb0e56c2ec3d7f96"
  },
  {
    "url": "platforms/Wii U/index.html",
    "revision": "f4dfee36cee36db6f11a1c878811927a"
  },
  {
    "url": "platforms/Wii/index.html",
    "revision": "1570f140d09b3c183a9a5188bc72341c"
  },
  {
    "url": "platforms/Xbox 360/index.html",
    "revision": "c3a61a782840aa49c08b344237c412bc"
  },
  {
    "url": "platforms/Xbox One/index.html",
    "revision": "adc84aa585107e15fde52e575cd877e9"
  },
  {
    "url": "platforms/Xbox/index.html",
    "revision": "55d55604d3e42b109c0968b4d47fba3d"
  },
  {
    "url": "README/index.html",
    "revision": "1e6c0475080ea9d5aef2e797cd32859f"
  },
  {
    "url": "tags/2D/index.html",
    "revision": "db8f5281eaceb6cc1cd65dde5f7952b5"
  },
  {
    "url": "tags/3D/index.html",
    "revision": "0879800f204a682852a37ddf5d9e589c"
  },
  {
    "url": "tags/Anime/index.html",
    "revision": "2e4af0a723bfbf8c22f9cc88547db506"
  },
  {
    "url": "tags/Arguable/index.html",
    "revision": "004d474c7b6683d695a73bd421bc259b"
  },
  {
    "url": "tags/Cameos/index.html",
    "revision": "2f8453fa94c70b211bda5df648ed7bbf"
  },
  {
    "url": "tags/Casual/index.html",
    "revision": "4023a7732a09934f08ac558e64d0626a"
  },
  {
    "url": "tags/Competitive/index.html",
    "revision": "077bfa79682ff2558a03af38ddb19fc7"
  },
  {
    "url": "tags/Crossover/index.html",
    "revision": "47a3a0ba3efd8eee65685eb243327bb2"
  },
  {
    "url": "tags/Custom Characters/index.html",
    "revision": "35d46a725ce9bf3b7ba44834a7ed846f"
  },
  {
    "url": "tags/F2P/index.html",
    "revision": "b06aebbfd0106c8c0fa6975c2de573ca"
  },
  {
    "url": "tags/game/index.html",
    "revision": "688400da614159feb00c8b0da5a6b31a"
  },
  {
    "url": "tags/Guns/index.html",
    "revision": "64647b0c23bf9aa3d36a4fc53e9f2124"
  },
  {
    "url": "tags/index.html",
    "revision": "e9be89d844ded49fef8d939323e30118"
  },
  {
    "url": "tags/Indie/index.html",
    "revision": "f92a6674d72bb6fba7816d6e8f908cdd"
  },
  {
    "url": "tags/JRPG/index.html",
    "revision": "eb28ea9d2f5756ca8360364b126c6e1c"
  },
  {
    "url": "tags/Licensed/index.html",
    "revision": "7db1fc982dd85c9056d6533640414c88"
  },
  {
    "url": "tags/Multiplayer Only/index.html",
    "revision": "3db737814e2acc20630f30d4f8fd17a5"
  },
  {
    "url": "tags/Pixel Art/index.html",
    "revision": "561f75a6d824979078182d10d8840faa"
  },
  {
    "url": "tags/platformCollections/index.html",
    "revision": "8bc734c10ced9a2739b19b6ba64da2c5"
  },
  {
    "url": "tags/Projectile/index.html",
    "revision": "16e5ac853c42d9e2c7b142ada4a36ce2"
  },
  {
    "url": "tags/Retro/index.html",
    "revision": "aad2a8a9fd8f057ca3453243b03a0349"
  },
  {
    "url": "tags/Spinoff/index.html",
    "revision": "cd93218e1b69ffaff405299e68317017"
  },
  {
    "url": "tags/Sports/index.html",
    "revision": "23512b2070bb136f1c5d8978f7d546da"
  },
  {
    "url": "tags/Story Mode/index.html",
    "revision": "88acfe36a522e61b0d7e5ad5cc89e46b"
  },
  {
    "url": "tags/Technical/index.html",
    "revision": "37f039c4f52fe64fcb0c185e24127413"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerRoute(/^.*\.(html|jpg|png|gif|webp|ico|svg|woff2|woff|eot|ttf|otf|ttc|json)$/, new workbox.strategies.StaleWhileRevalidate(), 'GET');
workbox.routing.registerRoute(/^https?:\/\/fonts\.googleapis\.com\/css/, new workbox.strategies.StaleWhileRevalidate(), 'GET');
